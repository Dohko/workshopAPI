var express = require('express');
const router = express.Router();
const Task = require('../controllers/task_controller.js');
const taskModel  = require('../models/taskModel.js');


//Taks
router.post('/task', Task.create);
router.get('/task/:id', Task.getUser);
router.patch('/task/:id', Task.update);
router.delete('/task/:id', Task.delete);

module.exports = router;