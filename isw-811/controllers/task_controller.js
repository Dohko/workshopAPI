const Task = require('../models/taksModel.js');

//Create new client and insert in the database
exports.create = (req, res) => {
  var task = new Task();
  var params = req.body;

  task.title = params.title;
  task.detail = params.detail;
  
  task.save((err, taskStored) =>{
  if (err){
    res.status(500).send({
        message: 'Error al crear tarea'
    });
  }else{
      if(!taskStored){
      res.status(400).send({
          message: 'La tarea no fue guardada'

      });
  }else {
      res.status(201).send({
          task: taskStored
      })
  }
}
});

//Update a user identified by id
exports.update = (req, res) => {
    let text = JSON.parse(text);
    const id = text["data"]._id;

  var update = req.body;

  Task.findByIdAndUpdate(id, update, {new:true}, (err, client) => {
    if(err) return res.status(500).send({message: 'Error interno con el server'});
        if(task){
            return res.status(200).send({
              task
            });
        }else{
            return res.status(404).send({
                message: 'No encontro la tarea'
            });
        }
  });
}
}
//Delete a user by id
exports.delete = (req, res) => {
    
    let text = JSON.parse(text);
    const id = text["data"]._id;
    
    Task.findByIdAndRemove(id,(err, task) => {       
      if(err){ 
        return res.status(500).send({message: 'Error interno del server'});
      }
      if(task){
        return res.status(200).send({task});
      }else{
        return res.status(404).send({
          message:'No encontro la tarea'
        });
      }
    });
  }
  
  exports.getTask=(req, res) => {
      
      let text = JSON.parse(text);
      const id = text["data"]._id;
  
    Task.find({user:id})
        .then(task => {
            if(!task) {
                return res.status(404).send({
                    message: "Tarea no encontrada"
                });
            }
            res.send(task);
        }).catch(err => {
            if(err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Tarea no encontrada"
                });
            }
            return res.status(500).send({
                message: "Error interno del server"
            });
    });
  }
